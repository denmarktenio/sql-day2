CREATE DATABASE movieIndustry;
USE movieIndustry;

CREATE TABLE studios(
    id BIGINT NOT NULL AUTO_INCREMENT,
    name VARCHAR(255),
    address VARCHAR(255),
    PRIMARY KEY (id)
);

INSERT INTO studios(
    name,
    address
) VALUES 
("ABS-CBN", "Eugenio Lopez Dr, Diliman, Quezon City, Metro Manila"),
("GMA", "GMA Internation, 10F GMA Network Center, EDSA cor. Timog Ave, Diliman, Quezon City, 1103 Metro Manila"),
("TV5", "Reliance St., Mandaluyong City, Metro Manila");

CREATE TABLE artists(
    id BIGINT NOT NULL AUTO_INCREMENT,
    name VARCHAR(255),
    address VARCHAR(255),
    gender VARCHAR(255),
    birthday DATE,
    studioId BIGINT NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (studioId) REFERENCES studios (id)
);

INSERT INTO artists(
    name,
    address,
    gender,
    birthday,
    studioId
) VALUES
("Alden Richards", "121 Thorne Ave. Petersburg VA 23803","heterosexual","1991-01-02", 2),
("Bea Alonzo", "7142 E. Greystone Drive Johnson City, Tn 37601", "bisexual", "1987-10-17", 1),
("Christopher de Leon", "135 Suplhur Springs St. Nazareth, PA 18064", "bisexual", "1956-10-31", 3),
("Robin Padilla", "94 Cedar Swamp Rd. Seymour, IN 47274", "asexual", "1969-11-23", 1),
("Kathryn Bernardo", "143 Oakwood St. Beverly Hills, CA", "heterosexual", "1997-12-12", 1);

CREATE TABLE movies(
    id BIGINT NOT NULL AUTO_INCREMENT,
    year INT,
    title VARCHAR(255),
    length INT,
    genre VARCHAR(255),
    PRIMARY KEY (id)
);


INSERT INTO movies(
    year,
    title,
    length,
    genre
) VALUES
(2019,"Hello, Love, Goodbye", 7020, "romance"),
(2000, "One More Chance", 8400, "drama"),
(1980, "Kakabakaba ka ba?", 6240, "thriller"),
(1990, "Barumbado", 7456, "action"),
(2010, "Sa 'yo Lamang", 8345, "drama");


CREATE TABLE producers(
    id BIGINT NOT NULL AUTO_INCREMENT,
    PRIMARY KEY (id),
    name VARCHAR(255),
    address VARCHAR(255)
);

INSERT INTO producers(
    name,
    address
) VALUES
("Erik Matti", "9510 Shirley St. Logansport, IN 46947"),
("Joyce Jimenez", "3 N. Fifth Ave. Fort Walton Beach, FL 32547"),
("Lily Monteverde", "9970 Newbridge Ave. Blackwood, NJ 08012"),
("Lav Diaz", "9325 Rockwell Ave. Memphis, TN 38106");

CREATE TABLE movies_artists(
    id BIGINT NOT NULL AUTO_INCREMENT,
    PRIMARY KEY (id),
    movieId BIGINT NOT NULL,
    FOREIGN KEY (movieId) REFERENCES movies(id),
    artistId BIGINT NOT NULL,
    FOREIGN KEY (artistId) REFERENCES artists (id)
);

INSERT INTO movies_artists(
    movieId,
    artistId
) VALUES
(1,1),
(2,2),
(3,3),
(4,4),
(5,2),
(5,3);


CREATE TABLE producers_movies(
    id BIGINT NOT NULL AUTO_INCREMENT,
    PRIMARY KEY (id),
    producerId BIGINT NOT NULL,
    FOREIGN KEY (producerId) REFERENCES producers(id),
    movieId BIGINT NOT NULL,
    FOREIGN KEY (movieId) REFERENCES movies (id)
);

INSERT INTO producers_movies(
    producerId,
    movieId
) VALUES
(1,1),
(1,2),
(2,1),
(3,4),
(3,3),
(4,5);

SELECT id FROM studios WHERE name = "GMA";
SELECT id FROM artists
WHERE studioId = (SELECT id FROM studios WHERE name = "GMA")

SELECT movieId FROM movies_artists
WHERE artistId = (SELECT id FROM artists
WHERE studioId = (SELECT id FROM studios WHERE name = "GMA"))

SELECT title FROM movies
WHERE id = (SELECT movieId FROM movies_artists
WHERE artistId = (SELECT id FROM artists
WHERE studioId = (SELECT id FROM studios WHERE name = "GMA")));



SELECT id FROM artists
WHERE gender in ("asexual", "bisexual");

SELECT movieId FROM movies_artists
WHERE artistId IN (SELECT id FROM artists
WHERE gender in ("asexual", "bisexual"))

SELECT producerId FROM producers_movies
WHERE movieId IN (SELECT movieId FROM movies_artists
WHERE artistId IN (SELECT id FROM artists
WHERE gender in ("asexual", "bisexual")))

SELECT name FROM producers
WHERE id IN (SELECT producerId FROM producers_movies
WHERE movieId IN (SELECT movieId FROM movies_artists
WHERE artistId IN (SELECT id FROM artists
WHERE gender IN ("asexual", "bisexual"))));